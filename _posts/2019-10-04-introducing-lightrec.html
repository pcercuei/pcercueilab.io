---
layout: "post"
title: "Introducing Lightrec, a MIPS-to-everything dynarec"
date: "2019-10-04 19:53:00 +0200"
categories: [Emulation]
---

<p>
Emulation is what got me into computer science to begin with, as I always thought that emulators are impressive pieces of software.
The fact that we simulate a real-world electronic device is just amazing.
What astounds me even more is that some emulators break the boundaries of what we thought was possible.
Who remembers UltraHLE? Who ever tried Bleem!cast?
As my knowledge of computer science increased for the last 12 years learning C, working on Linux and doing low-level programming on embedded systems,
emulators slowly ceased to be a mystery to me; but that made me even more respectful now that I can grasp the genius that went into these craft pieces.
</p>
<p>
The biggest praise I have for emulator creators is that they don&apos;t follow the common premise that the solution is always better hardware.
In a world where consumption is the key to our doom, I like to believe that we can always do more with less. Under constraints, people get creative.
Writing software on infinitely powerful machines would be boring.
</p>

<h2>Introducing Lightrec</h2>
Since 2014, I&apos;ve been working on-off on a project called <a href="https://github.com/pcercuei/lightrec">Lightrec</a>.
Started as an experiment, to test my skills and improve my knowledge, it later became a fully working dynamic recompiler (aka. <i>dynarec</i>)
for the PCSX Playstation emulator targetting a wide panel of host CPUs, thanks to the use of GNU Lightning as the code emitter.

<h3>Succeeding where others failed</h3>
<p>The big disavantage of traditional dynamic recompilers is that they only target one architecture.
PCSX has one dynamic recompiler for x86 PCs, another one for ARM-based smartphones, and yet another one for MIPS.
Each new dynarec means a different code base, a different performance, a different compatibility.</p>

<p>Ever since projects like LLVM or libjit came out, several unrelated attempts have been made by different people to create a dynamic compiler
that would use these technologies to support a lot of different CPUs. Unfortunately, they all failed, as they soon discovered that these technologies were really
not well-suited to dynamic recompilers. The reason is that while they can generate well-optimized code at runtime, they were not designed to do
so in a tight schedule. A game&apos;s frame time is generally of about 16ms, and the recompiler sometimes needs to execute <i>thousands</i> of pieces of code
in that time frame, something that LLVM or libjit just cannot do.</p>

<p><a href="https://www.gnu.org/software/lightning/">GNU Lightning</a> is different than the two aforementioned projects as it has a different scope.
LLVM and libjit were designed for creating programming language
compilers or fast interpreters, and as such have the concept of <i>variables</i>, which is a construct that all programming languages share, but not something that
machine code has. Machine code manipulates <i>registers</i>.</p>

<p>GNU Lightning is better described as a <i>code emitter</i>. It offers you a finite number of virtual registers (the actual number depends on the architecture), and
a programming API that closely ressembles the instruction set of MIPS processors. All it does, is translate each virtual instruction and virtual registers to the
corresponding CPU instruction (or instructions) with the corresponding hardware registers. It doesn&apos;t perform any optimization (except very obvious
and easy ones), and does not provide register allocation facilities either. Thanks to being that simple, it is extremely fast at generating code, and is well
suited for a portable dynamic recompiler project, as it supports almost every CPU on which you&apos;d ever want to run a Playstation emulator.</p>

<h3>Implementation details</h3>
<p>As you may have guessed by now, the <i>Lightrec</i> name is a fusion of <i>GNU Lightning</i> and <i>recompiler</i>, as it&apos;s what it really is.
It could also be read as <i>Light Recompiler</i> and that wouldn&apos;t be wrong either.</p>

<p>From a compatibility standpoint, Lightrec is very compatible with only a handful of games showing glitches or bugs.
Regarding performance, it was truely abysmal a couple of years ago, being slower than PCSX&apos;s interpreter.
It is now a few times faster, thanks to a few tricks:
<ul>
		<li></p><b>High-level optimizations.</b><br>
				The MIPS code is first pre-compiled into a form of Intermediate Representation (IR). Basically, just a single-linked list of structures representing the instructions.
				On that list, several optimization steps are performed: instructions are modified, reordered, tagged; new meta-instructions can be added,
				for instance to tell the code generator that a certain register won&apos;t be used anymore.</li></p>
		<li><p><b>Run-time profiling with a built-in interpreter.</b><br>
				The first time the MIPS code will jump to a new address, Lightrec will emulate it with its built-in interpreter.
				The interpreter will then gather run-time information. For instance, whether a load/store will hit the BIOS area, the RAM, or a hardware register.
				The code generator will then use this information to generate direct read/writes to the emulated memories, instead of jumping to C for every call.</li></p>
		<li><p><b>Lazy compilation.</b><br>
				If the interpreter detects a block of code that would be very hard to compile properly (e.g. a branch with a branch in its delay slot),
				the block is marked as <i>not compilable</i>, and will always be emulated with the interpreter.
				This allows to keep the code emitter simple and easy to understand.</li></p>
		<li><p><b>Threaded compilation.</b><br>
				The code generator can optionally run in a different thread of execution. Instead of compiling a block of code right when we jump to it, Lightrec can
				add it to the working queue of the threaded compiler, and emulate the block of code using the interpreter in the meantime. This greatly reduces stutter
				in the games when a lot of code is being recompiled, as the main execution thread doesn&apos;t wait anymore for the compilation process to finish.</li></p>
		<li><p><b>Fast code LUT.</b><br>
				Coming from psx4all&apos;s <i>mipsrec</i> dynarec, the function block Look-Up Table (LUT) is now a huge array of the size of the
				Playstation&apos;s RAM, 2 MiB. It makes it extremely fast to obtain a pointer to generated code from its MIPS address, and extremely easy to mark a block
				of code as outdated - the generated code just writes NULL to the corresponding offset.</li></p>
</ul></p>

<h3>Big-Ass Debugger</h3>
<p>The tool I developped that helped build this dynarec from the ground up is called the <i>Big-Ass Debugger</i>.
The name comes from the fact that it doesn&apos;t try to do anything smart: it runs the interpreter and the dynarec in parallel, and every time a block of code is
executed, it will calculate a hash of all the registers and the whole RAM, thousands of times per frame, in the two instances of the emulator, and compare the results.
It is a slow process, but if a difference is found, emulation stops and the debugger reports what exactly has gone wrong, and where it went wrong.
This tool is what allowed me, from a state where
the code emitted for all MIPS instructions were calls to PCSX&apos;s interpreter, to write the dynarec progressively, instruction after instruction, while still making
sure that my code was fully working and compliant with the expected behaviour shown by the interpreter. To this day, I still use it to verify each optimization and
improvement made to the dynarec.</p>

<h2>Projects using Lightrec</h2>
<p>So far Lightrec has been plugged into a few different emulators:
<ul>
		<li><a href="https://github.com/pcercuei/pcsx_rearmed">PCSX-ReArmed</a>, which is the emulator I&apos;ve been using for developing Lightrec. Not the fastest, since
				the dynarec exits after each piece of recompiled code; but it supports the Big-Ass Debugger.</li>
		<li><a href="https://github.com/pcercuei/pcsx4all">pcsx4all</a>, which is the fastest for various reasons: the dynarec doesn&apos;t return as often to the main loop,
				and the BIOS/scratchpad/RAM and RAM mirror memories are memory-mapped to locations that are a much better fit for the generated code.</li>
		<li><a href="https://github.com/ZachCook/beetle-psx-libretro/tree/lightrec">Beetle</a>, which is a libretro core based on Mednafen. The Lightrec integration is much
				more recent and still incomplete, but it already is a strong contender to replace the slow interpreter that Beetle has been using since the beginning.</li>
</ul>
</p>

<h2>Future</h2>
<p>As it is now, the dynarec is already working really well and ready for prime time. Of course, it still has ways to go; I already have ideas about advanced optimizations
(or should I say optimizations <b class="nickname">senquack</b> suggested) but all the &quot;easy&quot; optimizations have already been done, and the benefit-over-work-needed
ratio is getting smaller and smaller. Also, the fact that it&apos;s been plugged into Beetle means that we may start seeing it running on all libretro-supported platforms,
which is something I definitely look forward to.</p>
<p>Overall, it&apos;s been a challenging project and I&apos;m glad that I could take it to a state where it is usable.</p>
<p>Till next time!</p>
