---
layout: "post"
title: "Update on GCW-Zero / RG-350 OpenDingux ports"
date: "2020-06-10 00:46:50 +0200"
categories: [OpenDingux]
---

<p>It&apos;s been a while since I last did a public update on the status of
OpenDingux for the GCW-Zero. Believe it or not, a lot has changed since the
last release in 2014 🙂. Truth be told, I never really stopped to work on it
since 2014, and the folks on the Freenode IRC channel #opendingux can
testify just how much work I&apos;ve been doing.</p>

<h2>Linux From Scratch</h2>

<p>All the GCW-Zero specific code is gone. Everything has been moved to
Device Tree now. Device Tree is a textual representation of all the hardware
present in a device. This means that with the right Device Tree files, in
theory it should be possible to boot a generic MIPS kernel (Debian anyone?)
on both the GCW-Zero, the RG-350, the old Dingoo A320 and the RetroMini.
How cool is that?</p>

<p>The Device Tree source file is divided in two parts, one SoC specific
(JZ4770) and one device specific (GCW-Zero). At compilation time, these
two source files are assembled into one single binary file, that will
be either compiled into the kernel, or loaded by the bootloader.</p>

<p>If you are curious about what it looks like, this is the current
Device Tree file for the GCW-Zero:
<ul>
		<li><a href="https://github.com/OpenDingux/linux/blob/jz-5.7-gcw0/arch/mips/boot/dts/ingenic/jz4770.dtsi">
						JZ4770-specific part of devicetree</a></li>
		<li><a href="https://github.com/OpenDingux/linux/blob/jz-5.7-gcw0/arch/mips/boot/dts/ingenic/gcw0.dts">
						GCW-Zero specific part of devicetree</a></li></p>
</ul>

<p>Without going as far as having one kernel to support all devices,
this work will allow one single kernel to support both the GCW-Zero
and the RG-350, as well as the other JZ4770 based handhelds, provided
somebody write the devicetree for these (and the board-specific
hardware, like the LCD screen, is supported by Linux).
It will also allow to extend the flasher software that&apos;s been used
to flash the RetroMini through USB, to be used to flash other handhelds
as well, since the flasher is basically a Linux kernel with a small
payload. We do not want users to have no choice but to crack open a
device to flash the internal SD, when there is the option to flash from
USB. Besides all of that, switching to Device Tree files was the only
way to go to support the OpenDingux devices in the upstream Linux kernel;
crappy platform-specific code is not welcome anymore in there.</p>

<p>For the same reason, most of the drivers that were in use in the old
3.12 kernel are gone, too.
The clock code, timer code, and drivers for the MMC/SD, DMA, LCD
and framebuffer, IPU, VPU, GPU, I2C, ADC, joystick, battery, USB,
PWM and backlight, power regulator chip, I2S/AC97 and audio codec are
brand new. The WiFi drivers are new too but weren&apos;t written by us.
Of the old 3.12 drivers, only the watchdog, RTC and interrupt
controller drivers are still around. Most of these new drivers now also
support multiple Ingenic SoCs, and will work on the Dingoo A320 (JZ4740),
the RetroMini (JZ4725B), and even the MIPS CI20 (JZ4780). They will most
likely also work on the RS-97 (JZ4760) with very little work needed,
if any.</p>

<p>This sums up to a total of 342 patches that have been accepted upstream,
and about two dozens still in the process of being upstreamed, for a diff
of about 15k lines added and 7.5k lines removed. This may not sound like
much, but the process of upstreaming code to the Linux kernel is a long
and complicated one. All these patches
went under peer review and saw many revisions before being merged.
To give an order of magnitude of the work involved, if we count
all the intermediary states of these patches, it sums up to more than
six thousand commits that were authored by me. In the meantime, I
became the de-facto Linux kernel maintainer for Ingenic SoCs, which
means that all patches that touch Ingenic specific code now have to
go through me, and need my ack to go forward.</p>

<h2>Current status</h2>

<p>From a usability standpoint, the work on the kernel is pretty much
done, almost every single feature of the 3.12 kernel is present in the
current development kernel based on 5.7; the only exception being the
&quot;linkdev&quot; feature, which allowed to map the face and shoulder
buttons to joystick events, as it was simply impossible to upstream for
being nothing more than a quick and dirty hack. With that said, the
feature could very well be re-introduced at a different level, for
instance as a feature within SDL itself; but the feature was never really
used so dropping it shouldn&apos;t be a problem.</p>

<p>Since 3.12, new features were added, too. The IPU now supports a
variety of RGB color formats (15/16/32-bit, RGB/BGR), packed YUV 4:2:2 and
planar YUV 4:1:1, 4:2:0, 4:2:2, and 4:4:4. It now also scales with bicubic
filtering with a configurable sharpness factor. Experimental support for
HDMI was added as well, with support to resolutions up to 720p, a terrible
image quality and no audio (for now). Support for hardware overlays has
been added too, so it should be possible to have an overlay shown whenever
something like the volume or brightness is modified.
All the improvements that were made on the RS-90 port of OpenDingux are also
present here, for instance a proper mass-storage mode using MTP, just like
on Android, to replace the need for a FTP client.</p>

<p>As the kernel is pretty much complete, most of the work left concerns
the userspace. Speaking about SDL, while IPU support is integrated into the
kernel, the framebuffer emulation of the DRM/KMS core does not allow it to
be used through the old fbdev API. As a result, SDL applications cannot yet
use the IPU, but it&apos;s a problem that needs to be fixed in SDL, not in
the kernel, either by adding a DRM/KMS backend to SDL, or by using a
SDL-to-SDL2 compat library and using the DRM/KMS backend of SDL2 (which has
its own share of challenges).</p>

<p>The second other point that needs work, is WiFi support. The old root
filesystem of OpenDingux used <i>ifconfig</i> and <i>iwconfig</i> to configure
the network, which have been deprecated for years now. The current root
filesystem uses the newer <i>iw</i> tool, but the <i>GCWConnect</i> WiFi
configuration tool wasn&apos;t updated yet to support it, and the overall
performance and stability of wireless links is unknown.</p>

<p>The missing fish is USB OTG. This feature allows to plug USB peripherals
like gamepads to the USB port. I think everyone who ever tried USB OTG on
their GCW-Zero have had a hardware failure at some point, myself included.
I understand the interest in having the handheld plugged to HDMI with a
couple of gamepads connected, but that just won&apos;t happen on the
GCW-Zero. I cannot take the risk of causing more hardware failures.
This is however not a problem on the RG-350, which does <b>not</b> use
USB OTG, but has a USB host port directly available as USB1. On the
GCW-Zero, this USB host port is used internally to connect the WiFi chip.</p>

<p>On the RG-350, the only thing missing right now is support for the right
joystick, but that&apos;s just a matter of cleaning up the existing code
and integrating it properly into the new ADC driver.</p>

<h2>Tasty numbers</h2>

<p>For people worried about performance, the overhead of the OS is slightly
lower, which means that games and emulators gain a few FPS with the 5.7
kernel vs. the old 3.12 kernel. When recompiled with the new toolchain, based
on GCC 9, there is an extra small gain that adds up. This is with our
super-secret optimization turned OFF, which once enabled, will give a good
10+% performance increase on top of that.
I/O performance drastically improved too: the read and write speeds to the
SD cards, internal or external, more than doubled between the 3.12 and 5.7
kernels.</p>

<p>Some areas regressed, unfortunately. The upstream GPU kernel driver
(etnaviv) along with the OpenGL ES layer (Mesa) that are used in the current
development builds of OpenDingux have had a lot of fixes for crashes or
graphical glitches, and is in many ways faster, but is also much slower than
the old driver in some specific areas. With that said, the numbers of the
old firmware should be taken with a grain of salt, since a lot of them are
not rendering properly.<p>
<p><img src="/assets/images/glmark2_2020.svg"></img></p>

<p>The new kernel comes with advanced debugging tools, like
performance counters and debug overlays, so debugging the issue shouldn&apos;t
be a problem, but that still has to be done. On the bright side, working
close to upstream means that when we find the bottleneck, they will probably
fix it for us.</p>

<h2>ETA?</h2>

<p>As usual, it is pretty difficult for me to give an estimate about when
the next version of the firmware for all the supported OpenDingux devices
will be ready. Working on OpenDingux always feels like taking two steps
forward then one step back. The IPU issue with SDL is hopefully the last
big rock to smash in what has been a very long and lonesome road. Then
will come the testing phase, to be extra-careful that everything that used
to work still works.</p>
